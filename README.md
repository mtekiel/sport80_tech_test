# GAME MANAGER

### THE GAME 

There are three types of buildings in this game:

- Castle
    - The Castle has a lifespan of 100 Hit Points.
    - When the Castle is hit, 10 Hit Points are deducted from it's lifespan.
    - If/When the Castle has run out of Hit Points, the city is taken regardless 
    of the state of the other buildings
    - There is only 1 Castle.
- House
    - Houses have a lifespan of 75 Hit Points.
    - When a House is hit, 20 Hit Points are deducted from it's lifespan.
    - There are 4 Houses.
- Farm
    - Farms have a lifespan of 50 Hit Points.
    - When a Farm is hit, 25 Hit Points are deducted from it's lifespan.
    - There are 4 Farms.

- Gameplay:

    - To play, a user is able to attack a random building.
    - The selection of a building is random.
    - Trebuchets are old and not too accurate tools therefore leave 10% chance for 
    missing the target in any given attack.
    - The result of each attack need to be displayed. (which building it hit/ missed)
    - When all the buildings destroyed, a success message is shown.

