<?php

namespace App\Building;

use LIB\BuildingAction\BuildingActions;

class Castle
{
    use BuildingActions;

    /**
     * @var int
     */
    public $health = 100;
    /**
     * @var int
     */
    public $damage = 10;
       
}

// I did not have acces to private variables $health and $damage so I have changed them in to public.
// The other way of sorting this problem would be writnig public methods - getters and setters.