<?php

namespace App\Building;

use LIB\BuildingAction\BuildingActions;

class Farm
{
    use BuildingActions;

    /**
     * @var int
     */
    public $health = 50;
    /**
     * @var int
     */
    public $damage = 25;

}
