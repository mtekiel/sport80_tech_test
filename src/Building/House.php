<?php

namespace App\Building;

use LIB\BuildingAction\BuildingActions;

class House
{
    use BuildingActions;

    /**
     * @var int
     */
    public $health = 75;
    /**
     * @var int
     */
    public $damage = 20;
   
}
