<?php

namespace App\Game;

use App\Building\Castle;
use App\Building\Farm;
use App\Building\House;

class Play {
    /**
     * @var array
     */
    private $buildings = [];

    /**
     * Play constructor.
     * @param int $castleCount
     * @param int $houseCount
     * @param int $farmCount
     */
    public function __construct($castleCount=0, $houseCount=0, $farmCount=0)
    {
        $this->buildCity($castleCount,$houseCount,$farmCount);
    }

    /**
     * @param int $repeat
     */
    public function attack ($repeat = 1):int
    {
        //Changed the loop into do..while to eliminate situation when destroyed building is still under attact  
        do {
            $keyHitted = array_rand($this->buildings);
            $building = $this->buildings[$keyHitted];
        } while ($building->health <= 0);
        
        $building->hit();
    
        return $keyHitted;
    }

    /**
     * @param $houseCount
     * @param $farmCount
     */
   public function buildCity($castleCount,$houseCount,$farmCount)
    {

        for ($i = 0; $i < $castleCount; $i++) {
            $this->buildings[] = new Castle();
        }
        
        for ($i = 0; $i < $houseCount; $i++) {
            $this->buildings[] = new House();
        }
        for ($i = 0; $i < $farmCount; $i++) {
            $this->buildings[] = new Farm();
        }
    }

    /**
     * @return array
     */
    public function getBuildings()
    {
        return $this->buildings;
    }

    /**
     * Method that allows us to pass to the class PLay the array containing actual state of buildings
     * @return array
     */
    public function setBuildings(array $buildingState): self
    {
        $this->buildings = $buildingState;
        return $this;
    }

    
   
}
