<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\RequestStack;


class GameService{    
    private $requestStack;
    
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

   //Method checking session data and calculating newGameId
   public function getNewGameId(){

        $gameNumbers = [];
        $session = $this->requestStack->getSession();

        $arrays = $session->all();
        foreach ($arrays as $key => $value) {

            if (substr("$key", 0, 4) == "game") {
               
                array_push($gameNumbers, substr($key, 4));
            } else {
                $this->gameId = 1;                
            }
        }

        if (empty($gameNumbers)) {                      
            $this->newGameId = 1;
            array_push($gameNumbers,0);
        }

        $this->newGameId = (max($gameNumbers) + 1);
       
        return $this->newGameId;
    }

     //Method checking session data and calculating actualGameId
    public function getActualGameId()
    {
        $gameNumbers = [];
        $session = $this->requestStack->getSession();

        $arrays = $session->all();
        foreach ($arrays as $key => $value) {

            if (substr("$key", 0, 4) == "game") {                
                array_push($gameNumbers, substr($key, 4));
            } else {
                $this->gameId = 1;
            }
        }
        $actualGameId = (max($gameNumbers));      
        
        return $actualGameId;
    }    
}