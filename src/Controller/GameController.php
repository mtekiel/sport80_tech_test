<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use App\Game\Play;
use Symfony\Component\HttpFoundation\Session\Session as SessionSession;
use App\Services\GameService;


class GameController extends AbstractController
{
    
    private $requestStack;
    private $gameId;
    private $gameService;
    
    public function __construct(RequestStack $requestStack, GameService $gameService)
    {
        $this->requestStack = $requestStack;
        $this->gameService = $gameService;
    }
    
    /**
     * @Route("/", name="main_page")
     */
    public function index()
    {
        return $this->redirectToRoute('game_manager');        
    }


    /**
     * Method starting a new game     
     * @Route("/newgame", name="new_game")
     */
    public function newGame()
    {
        
        $play = new Play;
        $play->buildCity(1, 4, 4);
        $buildings = $play->getBuildings();

        $this->newGameId = $this->gameService->getNewGameId();
        
        $this->gameId = $this->newGameId;
       
              
        return $this->render('game/newgame.html.twig', [
            'header' => 'New Game',
            'page_title' => "New Game",
            'game_number' =>  $this->gameId,
            'buildings' => $buildings
        ]);
    }

    /**
     * Method handling first shot of the trebuchet
     * @Route("/firstfire", name="fire_trebuchet")
     */
    public function firtsFire()
    {
        $play = new Play();
        $play->buildCity(1, 4, 4);
        $session = new SessionSession();
       
        $chanceToHit = random_int(0, 100);

        //We take 10% of chance to miss the building
        if ($chanceToHit > 10) {
            $buildings = $play->getBuildings();
            $keyHitted = $play->attack();

            if ($keyHitted == 0) {
                $buildingName = "Castle";
            } elseif ($keyHitted > 4) {
                $buildingName = "Farm";
            } else {
                $buildingName = "House";
            }
            $this->addFlash('success', $buildingName . ' no ' . $keyHitted . '  hitted');

            $session->set('current_state', $buildings);
            $currentState = $session->get('current_state');
        } else {
            $buildings = $play->getBuildings();
            $session->set('buildings_state', $buildings);
            $currentState = $session->get('buildings_state');

            $this->addFlash('success', 'Building missed');
        }
        
        $this->newGameId = $this->gameService->getNewGameId();

        $currentGameState = $session->get('current_state');
        $session->set('game' . $this->newGameId, $currentGameState);       

        return $this->render('game/first_fire_state.html.twig', [
            'page_title' => "Buildings state",
            'header' => 'Game ',
            'game_number' =>  $this->newGameId,
            'buildings' => $currentState

        ]);
    }

    /**
     * Method handling shots of the trebuchet apart of the first one
     * @Route("/fire_again/", name="fire_again")
     */
    public function fireAgain()
    {
        $session = $this->requestStack->getSession();
        $chanceToHit = random_int(0, 100);
        $previousState = $session->get('current_state');
        $play2 = new Play();

        if ($chanceToHit > 10) {

            $play2->setBuildings($previousState);
            $keyHitted = $play2->attack();

            if ($keyHitted == 0) {
                $buildingName = "Castle";
            } elseif ($keyHitted > 4) {
                $buildingName = "Farm";
            } else {
                $buildingName = "House";
            }

            $this->addFlash('success', $buildingName . ' no ' . $keyHitted . '  hitted');
        } else {
            $play2->setBuildings($previousState);
            $this->addFlash('success', 'Building missed');
        }
        
            $actualState = $play2->getBuildings();
        
            $actualGameId = $this->gameService->getActualGameId();
            
            $currentGameState = $session->get('current_state');
            $session->set('game' . $actualGameId, $currentGameState);
            $session->save();
               

        return $this->render('game/actual_state.html.twig', [
            'page_title' => "Actual state",
            'header' => 'Game ',
            'game_number' => $actualGameId,
            'buildings' => $actualState,
        ]);
    }

    /**
     * Method loading state of chosen game
     * @Route("/loaded/{id}", name="continue_loaded_game")
     */
    public function continueLoadedGame($id)
    {

        $session = $this->requestStack->getSession();
        $chanceToHit = random_int(0, 100);
        $previousState = $session->get('game'.$id);

        $play2 = new Play();

        if ($chanceToHit > 10) {

            $play2->setBuildings($previousState);
            $keyHitted = $play2->attack();

            if ($keyHitted == 0) {
                $buildingName = "Castle";
            } elseif ($keyHitted > 4) {
                $buildingName = "Farm";
            } else {
                $buildingName = "House";
            }

            $this->addFlash('success', $buildingName . ' no ' . $keyHitted . '  hitted');
        } else {
            $play2->setBuildings($previousState);
            $this->addFlash('success', 'Building missed');
        }

        $actualState = $play2->getBuildings();
        $actualGameId = $id;

        $session->set('game'.$actualGameId, $actualState);
        $session->save();


        return $this->render('game_manager/loaded_game.html.twig', [
            'page_title' => "Actual state",
            'header' => 'Game ',
            'game_number' => $actualGameId,
            'buildings' => $actualState,

        ]);
    }   

}
