<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;

class GameManagerController extends AbstractController
{
    private $requestStack;
   
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @Route("/manager", name="game_manager")
     */
    public function index(): Response
    {
        $session = $this->requestStack->getSession();

        $allGames = $session->all();
                 

        return $this->render('game_manager/index.html.twig', [
            'page_title' => 'GameManagerController',
            'header' => 'Game Manager',
            'all_games' => $allGames
        ]);
    }

    /**
     * Method loading chosen game
     * @Route("manager/load/{id}", name="load_game")
     */
    public function loadGame($id)
    {
        $session = $this->requestStack->getSession();
        $loadedGame = $session->get('game'.$id); 

        return $this->render('game_manager/loaded_game.html.twig', [
            'page_title' => 'Building State',
            'header' => 'Loaded Game ',
            'game_number' => $id,
            'buildings' => $loadedGame
        ]);
    }
}
