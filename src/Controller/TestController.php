<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;

class TestController extends AbstractController
{
    private $requestStack;
   
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * Method displays all games data. Not available from UI.  
     * @Route("/test", name="test_page")
     */
    public function index()
    {
        $session = $this->requestStack->getSession();

        echo "<pre>";
        var_dump($session->all());
        die;       
    }

    /**
     * Method clears games data from session. Not available from UI.
     * @Route("/clear", name="clear_sesssion")
     */
    public function clearSession()
    {        
        $session = $this->requestStack->getSession();
        $session->clear();        
      
        return $this->redirectToRoute('test_page');  
    }
}




