<?php

namespace LIB\BuildingAction;

trait BuildingActions
{
    public function hit()
    {        
        $this->health -= $this->damage;    
    }
}